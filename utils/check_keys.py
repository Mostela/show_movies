import os

from error import EnvNotFound, NotIsFolder


class CheckKeys:
    @staticmethod
    def folder() -> bool:
        if os.getenv("PORN_FOLDER"):
            if os.path.isdir(os.getenv("PORN_FOLDER")):
                return True
            raise NotIsFolder(os.getenv("PORN_FOLDER"))
        else:
            raise EnvNotFound(os.getenv("PORN_FOLDER"))
