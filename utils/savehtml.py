import os

from utils.readfile import ReadFile


class SaveHtml(ReadFile):

    @staticmethod
    def execute(content, file_name):
        if not os.path.isfile(file_name):
            file = open(file_name, 'w')
            file.write(content)
            file.close()
