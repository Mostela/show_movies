import os

import requests


def download_file(url, name):
    local_filename = f"{os.getenv('PORN_FOLDER')}video/{name}.mp4"
    r = requests.get(url, stream=True)
    with open(local_filename, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
    return local_filename
