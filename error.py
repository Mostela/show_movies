from typing import Tuple


def _lstrip_str(in_s: str, lstr: str) -> str:
    if in_s.startswith(lstr):
        res = in_s[len(lstr):]
    else:
        res = in_s
    return res


class ShowVideoError(Exception):
    # Apparently the base class Exception already has __dict__ in it, so its not included here
    __slots__ = ('message',)

    def __init__(self, message: str):
        super().__init__()

        msg = _lstrip_str(message, 'Error: ')
        msg = _lstrip_str(msg, '[Error]: ')
        msg = _lstrip_str(msg, 'Bad Request: ')
        if msg != message:
            # api_error - capitalize the msg...
            msg = msg.capitalize()
        self.message = msg

    def __str__(self) -> str:
        return '%s' % self.message

    def __reduce__(self) -> Tuple[type, Tuple[str]]:
        return self.__class__, (self.message,)


class EnvNotFound(ShowVideoError):
    """raise when env not found"""
    __slots__ = ()

    def __init__(self, message: str):
        super().__init__(f"{message} env not found")


class NotIsFolder(ShowVideoError):
    __slots__ = ()

    def __init__(self, message: str):
        super().__init__(f"{message} not is folder or not exist")
