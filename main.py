import random

import webview

from api.goodfuck import GoodFuckAPI
from provider.goodfuck import GoodFuckProvider
from utils.check_keys import CheckKeys
from view.goodfuck import GoodFuckView

if __name__ == '__main__':
    try:
        main_category_id = random.randint(1, 80)
        video_number = random.randint(10000, 150000)
        CheckKeys.folder()
        url_search = f"https://www.boafoda.com/posts/load_related_posts/{main_category_id}/{video_number}"
        print("URL:", url_search)
        goodfuck = GoodFuckProvider(url=url_search)
        htmlview = GoodFuckView(title="Video", data_render=goodfuck.get_data())
        api = GoodFuckAPI()
        main_screen = webview.create_window(htmlview.title, html=htmlview.render(), js_api=api)
        webview.start()
    except KeyboardInterrupt as key:
        exit(0)
    except Exception as ex:
        print(ex)
        exit(1)

