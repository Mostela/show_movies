import abc


class HtmlBase(metaclass=abc.ABCMeta):
    def __init__(self, title, data_render):
        self._title = title
        self._data_render = data_render

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self, value):
        self._title = value

    @property
    def data_render(self):
        return self._data_render

    @data_render.setter
    def data_render(self, data):
        self._data_render = data

    def render(self):
        return f"""<!DOCTYPE html><html><head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>{self.title}</title>
    <body>{self._data_render}
        <script>
            function callme(id){{
                pywebview.api.callme(id).then()
            }}
        </script>
    </body></html>"""
