from bs4 import BeautifulSoup

from provider.site import SiteProvider


class GoodFuckVideoProvider(SiteProvider):
    def __init__(self, url):
        super().__init__(url)

    @property
    def url(self):
        return super().url()

    def get_data(self):
        html = super().get_data()
        return self.get_link(html)

    def get_link(self, html):
        soup = BeautifulSoup(html, features="html.parser")
        elements = soup.find_all("a", class_="post_download_link clearfix")
        if len(elements) >= 4:
            return elements[3].attrs['href']
        elif len(elements) == 2:
            return elements[1].attrs['href']
        else:
            return elements[0].attrs['href']
