import abc
import requests


class SiteProvider(metaclass=abc.ABCMeta):
    def __init__(self, url):
        self._url = url

    @property
    def url(self):
        return self._url

    @url.setter
    def url(self, value):
        self._url = value

    def get_data(self):
        try:
            if self._url is None:
                raise ValueError(f"URL not set from provider {__name__}")
            data = requests.get(self._url)
            if data.status_code != 200:
                print(data.status_code, data.text)
                return data.text
            else:
                return data.text
        except requests.RequestException:
            print(f"request failed to call {self._url}")
        except Exception as ex:
            print(f"Failed in provider { __name__ } - {ex}")
