import abc


class AbstractAPI(metaclass=abc.ABCMeta):
    @staticmethod
    @abc.abstractmethod
    def callme(url_image: str, name_file: str):
        pass
