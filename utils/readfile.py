import abc
import os


class ReadFile(metaclass=abc.ABCMeta):
    @staticmethod
    @abc.abstractmethod
    def execute(content, file_name):
        if os.path.isfile(file_name):
            file = open(file_name, 'r')
            data = file.read()
            file.close()
            return data
